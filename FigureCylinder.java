package FigureSquareCircle;

/**
 * Created by Yurii on 17-Sep-17.
 */
public class FigureCylinder extends FigureCircle {

    public int calcCylSquare(int radius, int height){
        int cylVolume = calcSquare(radius) * height;
        if (radius<=0 | height <=0)
            throw new IllegalArgumentException("Incorrect Character");
        return cylVolume;
    }
}
