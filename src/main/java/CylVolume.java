package TestJunitCircle;
import FigureSquareCircle.FigureCircle;
import FigureSquareCircle.FigureCylinder;
import org.junit.Assert;
import org.junit.Test;

public class CylVolume {

    @Test
    public void figureCircleSquarePositive(){
        FigureCylinder cyl = new FigureCylinder();
        Assert.assertEquals(cyl.calcCylSquare(2,5), 62);
    }

    @Test(expected = IllegalArgumentException.class)
    public void figureCircleSquareFalse(){
        FigureCylinder cyl = new FigureCylinder();
        cyl.calcCylSquare(0,0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void figureCircleFalseFalse1(){
        FigureCylinder cyl = new FigureCylinder();
        cyl.calcCylSquare(2, -10);
    }
}

